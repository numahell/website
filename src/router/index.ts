
import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Spheres from '@/components/spheres/Spheres.vue';
import Rejoindre from '@/components/Rejoindre.vue';
import Decouvrir from '@/components/Decouvrir.vue';
import Description from '@/components/Description.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  }, {
    path: '/rejoindre',
    name: 'join',
    component: Rejoindre,
  }, {
    path: '/decouvrir/:category',
    name: 'discover',
    component: Decouvrir,
  },
  {
    path: '/spheres',
    name: 'spheres',
    component: Spheres,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/description',
    name: 'description',
    component: Description,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
