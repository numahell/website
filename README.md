# joinfediverse le site


## installation locale sur windows
à lancer dans un Git bash
disposer de [Git](https://git-scm.com/download/win) et [Npm](https://nodejs.org/en/download/) (livré avec NodeJS) sur sa machine.
```bash
git clone https://framagit.org/tykayn/joinfediverse.git
cd joinfediverse
npm i -g yarn
yarn install
yarn serve
```
ouvrir ensuite votre navigateur respectueux de la vie privée favori, par exemple Firefox à l'URL http://localhost:8080
``` 
http://localhost:8080
```
## Installation dans un système Ubuntu/debian
### Dépendances Node et Git

```bash
curl -L https://www.npmjs.com/install.sh | sh
sudo apt install git

git clone https://framagit.org/tykayn/joinfediverse.git
cd joinfediverse
npm i -g yarn
yarn install
yarn serve
```


# déploiement
il faut builder le projet et se monter un serveur. Demandez de l'aide à un CHATONS.


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
